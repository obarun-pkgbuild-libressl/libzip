# Maintainer  : Jean-Michel T.Dydak <jean-michel@obarun.org> <jean-michel@syntazia.org>
#--------------
## PkgSource  : https://www.archlinux.org/packages/extra/x86_64/libzip/
## Maintainer : Andrea Scarpino <andrea@archlinux.org>
## Contributor: Tobias Powalowski <tpowa@archlinux.org>
#--------------------------------------------------------------------------------------

pkgname=libzip
pkgver=1.5.2
pkgrel=2
arch=('x86_64')
license=('BSD')
_website="https://libzip.org"
pkgdesc="A C library for reading, creating, and modifying zip archives"
url="https://libzip.org/download"
source=("$url/$pkgname-$pkgver.tar.xz")

depends=(
    'zlib'
    'bzip2'
    'libressl')
makedepends=(
    'cmake')

#--------------------------------------------------------------------------------------
build() {
    cd "$pkgname-$pkgver"

    mkdir build && cd build

    cmake \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DCMAKE_INSTALL_LIBDIR=lib  \
        ..
    make
}

package() {
    cd "$pkgname-$pkgver"/build

    make DESTDIR="${pkgdir}" install
    install -Dm644 ../LICENSE "${pkgdir}/usr/share/licenses/$pkgname/LICENSE"
}

#--------------------------------------------------------------------------------------
sha512sums=('1e6d48ddbac4d270f70b314f6ada3c93a3196a8cc3b8d17c6cf5ea8409ff29f36ac351206675f9d81351fcf633b0c15d7b0b5530c30c4140e4fe55e64c602643')
